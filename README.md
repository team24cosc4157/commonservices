# README

### Summary

* Common services files shared between servers.

### Navigation

* `index.css`
    * Styling for HTML pages.
* `index.html`
    * Main landing page for website, with links to other pages on both servers.

### Team members

* TM1 - Sydney Mack
* TM2 - Mariah Sager
* TM3 - Keldon Boswell
* TM4 - Colby Hayes
* TM5 - Karla Salto
